var exec = require("cordova/exec");
var PLUGIN_NAME = "FirebaseAnalytics";

module.exports = {
    logEvent: function(name, params, successcb, failedcb) {
        exec(successcb, failedcb, PLUGIN_NAME, "logEvent", [name, params || {}]);
    },
    setUserId: function(userId) {
        exec(resolve, reject, PLUGIN_NAME, "setUserId", [userId]);
    },
    setUserProperty: function(name, value) {
        exec(resolve, reject, PLUGIN_NAME, "setUserProperty", [name, value]);
    },
    resetAnalyticsData: function() {
        exec(resolve, reject, PLUGIN_NAME, "resetAnalyticsData", []);
    },
    setEnabled: function(enabled) {
        exec(resolve, reject, PLUGIN_NAME, "setEnabled", [enabled]);
    },
    setCurrentScreen: function(name) {
        exec(resolve, reject, PLUGIN_NAME, "setCurrentScreen", [name]);
    }
};
