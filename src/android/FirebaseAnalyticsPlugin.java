package by.chemerisuk.cordova.firebase;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;


public class FirebaseAnalyticsPlugin extends CordovaPlugin {
    private static final String TAG = "FirebaseAnalyticsPlugin";

    private FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void pluginInitialize() {
        Log.d(TAG, "Starting Firebase Analytics plugin");

        Context context = this.cordova.getActivity().getApplicationContext();

        this.firebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public boolean execute(final String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {

    		Log.d(TAG,"==> FCMPlugin execute: "+ action);

    		try{
    			if (action.equals("logEvent")) {
                    cordova.getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                final String name = args.get(0).toString();
                                final String params = args.get(1).toString();

                                Bundle bundle = new Bundle();
//                                Iterator<String> it = params.keys();
//
//                                while (it.hasNext()) {
//                                    String key = it.next();
//                                    Object value = params.get(key);
//
//                                    if (value instanceof String) {
//                                        bundle.putString(key, (String) value);
//                                    } else if (value instanceof Integer) {
//                                        bundle.putInt(key, (Integer) value);
//                                    } else if (value instanceof Double) {
//                                        bundle.putDouble(key, (Double) value);
//                                    } else if (value instanceof Long) {
//                                        bundle.putLong(key, (Long) value);
//                                    } else {
//                                        Log.w(TAG, "Value for key " + key + " not one of (String, Integer, Double, Long)");
//                                    }
//                                }

                                firebaseAnalytics.logEvent(name, bundle);

                                callbackContext.success();
                            }catch(JSONException e) {
                                Log.d(TAG, "ERROR: onPluginAction: " + e.getMessage());
                                callbackContext.error(e.getMessage());
                                return;
                            }
                        }
                    });
                } else {
                    callbackContext.error("Method not found");
                    return false;
                }
            }catch(Exception e){
                Log.d(TAG, "ERROR: onPluginAction: " + e.getMessage());
                callbackContext.error(e.getMessage());
                return false;
            }

            return true;
    }

}
